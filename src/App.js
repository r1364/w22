import './App.css';
import ComponentName from './ComponentName';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ComponentName country="Finland" />
      </header>
    </div>
  );
}

export default App;
